#pragma once
#include "Piece.h"
#include "Game.h"
class Pawn : public Piece
{
public:
	Pawn(char name, Game* board, int row, int col);
	~Pawn();
	virtual bool Move(int row, int col);
	void SetFirstTurn();
private:
	bool _firstTurn;
};