#pragma once
#include "Piece.h"
#include "Game.h"
class Rook : public Piece
{
public:
	Rook(char name, Game* board, int row, int col);
	~Rook();
	virtual bool Move(int row, int col);
};
