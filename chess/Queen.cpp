#include "Queen.h"
Queen::Queen(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col) {};
Queen::~Queen() {}
bool Queen::Move(int row, int col)
{
	char rookName = _color ? 'R' : 'r';
	char bishopName = _color ? 'B' : 'b';
	Rook rook(rookName, _board, _row, _col);
	Bishop bishop(bishopName, _board, _row, _col);
	return rook.Move(row, col) || bishop.Move(row, col);
	/* int checkRow = _row, checkCol = _col;
	bool checkPath = _row == row || _col == col || _row + _col == row + col || _row - _col == row - col;
	if (checkPath)
	{
		if (row > checkRow)
			checkRow++;
		else if (row < checkRow)
			checkRow--;
		else if (col > checkCol)
			checkCol++;
		else
			checkCol--;
		while (checkPath && (checkRow != row || checkCol != col))
		{
			checkPath = _board->CheckSquare(checkRow, checkCol) == 0;
			if (row > checkRow)
				checkRow++;
			else if (row < checkRow)
				checkRow--;
			else if (col > checkCol)
				checkCol++;
			else
				checkCol--;
		}
	}
	return checkPath; */
}