#pragma once
#include "Game.h"
#include "Bishop.h"
#include "Rook.h"
class Queen : public Piece
{
public:
	Queen(char name, Game* board, int row, int col);
	~Queen();
	virtual bool Move(int row, int col);
};