#include "Game.h"
#include "Piece.h"
Game::Game(const char* board)
{
	_board = new Piece**[8];
	for (int i = 0; i < 8; i++)
	{
		_board[i] = new Piece*[8];
		for (int j = 0; j < 8; j++)
		{
			if (tolower(board[i * 8 + j]) == 'r')
				_board[i][j] = new Rook(board[i * 8 + j], this, i, j);
			else if (tolower(board[i * 8 + j]) == 'n')
				_board[i][j] = new Knight(board[i * 8 + j], this, i, j);
			else if (tolower(board[i * 8 + j]) == 'b')
				_board[i][j] = new Bishop(board[i * 8 + j], this, i, j);
			else if (tolower(board[i * 8 + j]) == 'q')
				_board[i][j] = new Queen(board[i * 8 + j], this, i, j);
			else if (tolower(board[i * 8 + j]) == 'k')
				_board[i][j] = new King(board[i * 8 + j], this, i, j);
			else if (tolower(board[i * 8 + j]) == 'p')
				_board[i][j] = new Pawn(board[i * 8 + j], this, i, j);
			else
				_board[i][j] = nullptr;
		}
	}
	_turn = board[64] == 0;
}
Game::~Game()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			delete _board[i][j];
		}
		delete[] _board[i];
	}
	delete[] _board;
}
int Game::CheckSquare(int row, int col) const
{
	if (_board[row][col] == nullptr)
		return 0;
	if (_board[row][col]->GetColor())
		return 1;
	return 2;
}
int Game::CheckChess() const
{
	Piece* whiteKing = FindKing(true);
	Piece* blackKing = FindKing(false);
	if (whiteKing != nullptr && blackKing != nullptr)
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				if (_board[i][j] != nullptr)
				{
					if (!_board[i][j]->GetColor() && _board[i][j]->Move(whiteKing->GetRow(), whiteKing->GetCol()))
						return 2;
					if (_board[i][j]->GetColor() && _board[i][j]->Move(blackKing->GetRow(), blackKing->GetCol()))
						return 1;
				}
			}
		}
	}
	return 0;
}
Piece* Game::FindKing(bool color) const
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (_board[i][j] != nullptr && ((_board[i][j]->GetName() == 'K' && color) || (_board[i][j]->GetName() == 'k' && !color)))
				return _board[i][j];
		}
	}
	return nullptr;
}
char Game::Move(const char* squares)
{
	int srcCol = squares[0] - 'a', srcRow = '8' - squares[1], destCol = squares[2] - 'a', destRow = '8' - squares[3];
	Piece* srcPiece = nullptr, *destPiece = nullptr;
	if (srcRow == destRow && srcCol == destCol)
		return '7';
	if (srcRow > 7 || srcCol > 7 || destRow > 7 || destCol > 7)
		return '5';
	srcPiece = _board[srcRow][srcCol];
	if (srcPiece == nullptr || srcPiece->GetColor() == _turn)
		return '2';
	if (_board[destRow][destCol] != nullptr && _board[destRow][destCol]->GetColor() == srcPiece->GetColor())
		return '3';
	if (!srcPiece->Move(destRow, destCol))
		return '6';
	destPiece = _board[destRow][destCol];
	_board[destRow][destCol] = srcPiece;
	_board[srcRow][srcCol] = nullptr;
	srcPiece->SetRow(destRow);
	srcPiece->SetCol(destCol);
	if ((CheckChess() == 1 && !srcPiece->GetColor()) || (CheckChess() == 2 && srcPiece->GetColor()))
	{
		_board[destRow][destCol] = destPiece;
		_board[srcRow][srcCol] = srcPiece;
		srcPiece->SetRow(srcRow);
		srcPiece->SetCol(srcCol);
		return '4';
	}
	_turn = !_turn;
	if (tolower(srcPiece->GetName()) == 'p')
		dynamic_cast<Pawn*>(srcPiece)->SetFirstTurn();
	if (CheckChess() == 1 && srcPiece->GetColor() || CheckChess() == 2 && !srcPiece->GetColor())
		return '1';
	return '0';
}