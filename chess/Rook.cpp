#include "Rook.h"
Rook::Rook(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col) {};
Rook::~Rook() {}
bool Rook::Move(int row, int col)
{
	int checkRow = _row, checkCol = _col;
	bool checkPath = _row == row || _col == col;
	if (checkPath)
	{
		if (row > checkRow)
			checkRow++;
		else if (row < checkRow)
			checkRow--;
		else if (col > checkCol)
			checkCol++;
		else
			checkCol--;
		while (checkPath && (checkRow != row || checkCol != col))
		{
			checkPath = _board->CheckSquare(checkRow, checkCol) == 0;
			if (row > checkRow)
				checkRow++;
			else if (row < checkRow)
				checkRow--;
			else if (col > checkCol)
				checkCol++;
			else
				checkCol--;
		}
	}
	return checkPath;
}