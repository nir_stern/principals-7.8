#pragma once
#include "Piece.h"
#include "Game.h"
#include <iostream>
class King : public Piece
{
public:
	King(char name, Game* board, int row, int col);
	~King();
	virtual bool Move(int row, int col);
};