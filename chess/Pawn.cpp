#include "Pawn.h"
Pawn::Pawn(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col)
{
	_firstTurn = true;
}
Pawn::~Pawn() {}
bool Pawn::Move(int row, int col)
{
	if (_firstTurn)
		if ((_color && row == _row - 2 && _board->CheckSquare(_row - 1, _col) == 0 && _board->CheckSquare(_row - 2, _col) == 0) || (!_color && row == _row + 2 && _board->CheckSquare(_row + 1, _col) == 0 && _board->CheckSquare(_row + 2, _col) == 0))
			return true;
	if ((_color && row == _row - 1) || (!_color && row == _row + 1))
		if ((col == _col && _board->CheckSquare(row, col) == 0) || (_board->CheckSquare(row, col) != 0 && abs(col - _col) == 1))
			return true;
	return false;
}
void Pawn::SetFirstTurn()
{
	_firstTurn = false;
}