#pragma once
class Game;
class Piece
{
public:
	Piece(char name, Game* board, int row, int col);
	~Piece();
	char GetName() const;
	bool GetColor() const;
	int GetRow() const;
	int GetCol() const;
	void SetRow(int row);
	void SetCol(int col);
	virtual bool Move(int row, int col) = 0;
protected:
	bool _color;
	char _name;
	Game* _board;
	int _row;
	int _col;
};
