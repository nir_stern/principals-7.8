#pragma once
#include <iostream>
#include "Piece.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Pawn.h"
class Game
{
public:
	Game(const char* board);
	~Game();
	char Move(const char* squares);
	int CheckSquare(int row, int col) const;
private:
	int CheckChess() const;
	Piece* FindKing(bool color) const;

	Piece*** _board;
	bool _turn;
};
