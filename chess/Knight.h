#pragma once
#include "Piece.h"
#include "Game.h"
class Knight : public Piece
{
public:
	Knight(char name, Game* board, int row, int col);
	~Knight();
	virtual bool Move(int row, int col);
};