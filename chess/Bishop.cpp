#include "Bishop.h"
Bishop::Bishop(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col) {};
Bishop::~Bishop() {}
bool Bishop::Move(int row, int col)
{
	int checkRow = _row, checkCol = _col;
	bool checkPath = _row + _col == row + col || _row - _col == row - col;
	if (checkPath)
	{
		if (row > checkRow)
			checkRow++;
		else
			checkRow--;
		if (col > checkCol)
			checkCol++;
		else
			checkCol--;
		while (checkPath && checkRow != row)
		{
			checkPath = _board->CheckSquare(checkRow, checkCol) == 0;
			if (row > checkRow)
				checkRow++;
			else
				checkRow--;
			if (col > checkCol)
				checkCol++;
			else
				checkCol--;
		}
	}
	return checkPath;
}