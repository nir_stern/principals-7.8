#include "King.h"
King::King(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col) {};
King::~King() {}
bool King::Move(int row, int col)
{
	return abs(_row - row) <= 1 && abs(_col - col) <= 1;
}