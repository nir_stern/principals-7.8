#include "Piece.h"
#include "Game.h"
Piece::Piece(char name, Game* board, int row, int col)
{
	_color = isupper(name);
	_name = name;
	_board = board;
	_row = row;	
	_col = col;
}
Piece::~Piece() {}
char Piece::GetName() const
{
	return _name;
}
bool Piece::GetColor() const
{
	return _color;
}
int Piece::GetRow() const
{
	return _row;
}
int Piece::GetCol() const
{
	return _col;
}
void Piece::SetRow(int row)
{
	_row = row;
}
void Piece::SetCol(int col)
{
	_col = col;
}