#pragma once
#include "Piece.h"
#include "Game.h"
class Bishop : public Piece
{
public:
	Bishop(char name, Game* board, int row, int col);
	~Bishop();
	virtual bool Move(int row, int col);
};