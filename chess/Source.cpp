#include "Pipe.h"
#include "Game.h"
#include <iostream>
#include <thread>

using namespace std;
void main()
{
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();
	
	string ans = "0";
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE
	char* board = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
	Game* g = new Game(board);
	strcpy_s(msgToGraphics, board); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		ans[0] = g->Move(msgFromGraphics.c_str());
		
		// YOUR CODE
		strcpy_s(msgToGraphics, ans.c_str()); // msgToGraphics should contain the result of the operation




		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}
	delete g;
	p.close();
}