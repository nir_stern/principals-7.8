#include "Knight.h"
Knight::Knight(char name, Game* board, int row, int col) : Piece::Piece(name, board, row, col) {};
Knight::~Knight() {}
bool Knight::Move(int row, int col)
{
	return (abs(row - _row) == 1 && abs(col - _col) == 2) || (abs(row - _row) == 2 && abs(col - _col) == 1);
}